import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';
import './route.js';
import '../teamplate/component/toolbar.html';
import '../teamplate/layout/main_layout.html';
import '../teamplate/page/page1.html';
import '../teamplate/page/page2.html';

import '../teamplate/layout/main_layout.js';

// Template.hello.onCreated(function helloOnCreated() {
// 	// counter starts at 0
// 	this.counter = new ReactiveVar(0);
// });

// Template.hello.helpers({
// 	counter() {
// 		return Template.instance().counter.get();
// 	},
// });

// Template.hello.events({
// 	'click button'(event, instance) {
// 		// increment the counter when button is clicked
// 		instance.counter.set(instance.counter.get() + 1);
// 	},
// });
